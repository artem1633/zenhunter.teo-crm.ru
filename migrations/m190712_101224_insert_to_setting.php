<?php

use yii\db\Migration;

/**
 * Class m190712_101224_insert_to_setting
 */
class m190712_101224_insert_to_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings',array(
            'name' => 'Интервал обновления',
            'key' => 'time_interval',
            'value' => '3',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
