<?php

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use MetzWeb\Instagram\Instagram;
use app\models\Settings;
use app\models\UsersPage;
use app\models\PageBlocks;
use app\models\Users;
use app\models\additional\Permissions;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link href="/css/fontawesome-all.min.css" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php

    if(Yii::$app->controller->id == 'default' && Yii::$app->controller->action->id == 'index')
    {
        if (isset($_GET['user'])) 
        {
            //$user = Users::find()->where(['login' => $_GET['user']])->one();
            $user_page = UsersPage::find()->where(['link_name' => $_GET['user'] ])->one();
            if($user_page != null)
            {    
                $blocks = PageBlocks::find()->where(['page_id' => $user_page->id])->orderBy([ 'sorting' => SORT_ASC])->all();
                if($user_page->user->tariff->key == 'pro') $items = PageBlocks::getBlocksField($blocks);
                else $items = [];
                
                NavBar::begin([
                    'brandLabel' => Yii::$app->name,
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top',
                    ],
                ]);

                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right', 'id' => 'page-menu'],
                    'items' => $items,
                ]);

                NavBar::end();
            }
        }
    ?>
        <div class="container">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    <?php 
        
    }
    else {
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);

        $dostup = true;
        $type = Yii::$app->user->identity->type;
        $user_id = Yii::$app->user->identity->id;
        if(Yii::$app->user->identity->id == null) $dostup = false;
        if($type == 2)
        {
            $permission = Permissions::find()->where(['user_id' => $user_id])->one();
            if(!$permission->view_user) $dostup = false;
        }

        if(Yii::$app->user->isGuest == false){
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Анкеты', 'url' => ['/questionary/index']],
                    [
                        'label' => 'Администратор', 'url' => ['#'],
                        'visible' => Yii::$app->user->identity->type != 0 ? false : true,
                        'items'=>[
                            ['label' => 'Компании', 'url' => ['/users/index'], 'visible' => $dostup],
                            ['label' => 'Настройки', 'url' => ['/settings/index'],],
                        ],
                    ],
                    [
                        'label' => 'Настройки', 'url' => ['#'],
                        'items'=>[
                            ['label' => 'Уведомление', 'url' => ['/notification/index'], 'visible' => $dostup],
                            ['label' => 'Ваканция', 'url' => ['/vacancy/index'],],
                            ['label' => 'Группа', 'url' => ['/group/index'],],
                            ['label' => 'Статус резюме', 'url' => ['/resume-status/index'],],
                        ],
                    ],
                    
                    /*[
                        'label' => 'Профиль', 'url' => ['#'],
                        'items'=>[
                            ['label' => 'Личный кабинет', 'url' => ['/users/view', 'id' => Yii::$app->user->identity->id]],
                            ['label' => 'Шаблоны страниц', 'url' => ['/#']],
                        ],
                    ],*/
                    Yii::$app->user->isGuest ? (
                        ['label' => 'Login', 'url' => ['/site/login']]
                    ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Выход',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    )
                ],
            ]);
        }
        else{
            /*/////--------------------------------------Instagram--------------------------------------------//////
            $instagram = new Instagram(array(
                'apiKey'      => Settings::find()->where(['key' => 'instagram_client_id'])->one()->value,
                'apiSecret'   => Settings::find()->where(['key' => 'instagram_client_secret_key'])->one()->value,
                'apiCallback' => Settings::find()->where(['key' => 'instagram_api_callback'])->one()->value,
            ));
            /////---------------------------------------VK---------------------------------------------------/////
            $app_id = Settings::find()->where(['key' => 'vk_id_app'])->one()->value;
            $my_url = Settings::find()->where(['key' => 'vk_api_callback'])->one()->value;
            $dialog_url = 'https://oauth.vk.com/authorize?client_id='.$app_id.'&redirect_uri='.$my_url.'&response_type=code&display=page&scope=nohttps,groups,photos,friends,offline';
            /////---------------------------------------FaceBook---------------------------------------------------//////
            $client_id  = Settings::find()->where(['key' => 'facebook_client_id'])->one()->value;
            $client_secret  = Settings::find()->where(['key' => 'facebook_client_secret'])->one()->value;
            $redirect_uri  = Settings::find()->where(['key' => 'facebook_redirect_uri'])->one()->value;

            $url = 'https://www.facebook.com/dialog/oauth';

            $params = array(
                'client_id'     => $client_id,
                'redirect_uri'  => $redirect_uri,
                'response_type' => 'code',
                'scope'         => 'email,user_birthday'
            );
            $url_facebook = $url . '?' . urldecode(http_build_query($params));
            //
            //------------------------------Google Api----------------------------------------//
            $client_id_google = Settings::find()->where(['key' => 'google_client_id'])->one()->value;
            $client_secret_key = Settings::find()->where(['key' => 'google_client_secret_key'])->one()->value;
            $redirect_uri_google = Settings::find()->where(['key' => 'google_redirect_uri'])->one()->value;

            $url_google = 'https://accounts.google.com/o/oauth2/auth';

            $params_google = array(
                'redirect_uri'  => $redirect_uri_google,
                'response_type' => 'code',
                'client_id'     => $client_id_google,
                'scope'         => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
            );*/
            //-------------------------------------------------------------------------------//

            /*echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    [
                        'label' => 'Авторизация через', 'url' => ['#'],
                        'items'=>[
                            ['label' => 'Войти через Instagram', 'url' => $instagram->getLoginUrl()],
                            ['label' => 'Войти через VK', 'url' => $dialog_url],
                            ['label' => 'Войти через FaceBook', 'url' => $url_facebook],
                            ['label' => 'Войти через Google', 'url' => $url_google. '?' . urldecode(http_build_query($params_google)) ],
                            ['label' => 'Регистрация по е-мэйл', 'url' => ['/site/register']],
                        ],
                    ],
                ],
            ]);*/
        }
            NavBar::end();
        ?>

        <div class="container">
            <?php /*Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])*/ ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    <?php } ?>
</div>
<?php 
    if(Yii::$app->controller->id != 'default')
    {
?>
    <footer class="footer">
        <div class="container">
            <p class="pull-left">Copyright &copy; TEO-CRM <?= date('Y') ?> Данный проект сделан компанией <a href="https://shop-crm.ru/"> <b>Technology to EveryOne</b></a>. All rights reserved.</p>
        </div>
    </footer>
<?php } ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
