<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%reports}}`.
 */
class m190704_155431_create_reports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%reports}}', [
        'id' => $this->primaryKey(),
        'link' => $this->string(255)->comment('URL'),
        'type' => $this->integer()->comment('тип'),
        'title' => $this->string(255)->comment('Заголовок'),
        'date' => $this->date()->comment('Дата публикации'),
        'view_count' => $this->integer()->comment('Количество просмотров'),
        'reading_count' => $this->integer()->comment('Количество дочитываний'),
        'reading_prosent' => $this->float()->comment('Процент дочитываний '),
        'average_time' => $this->float()->comment('Среднее время дочитывания'),
        'like_count' => $this->float()->comment('Количество лайков'),
        'comment_count' => $this->float()->comment('Количество комментариев'),
        'status' => $this->integer()->comment('Статус'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%reports}}');
    }
}
