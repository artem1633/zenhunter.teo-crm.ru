<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

$path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
$pathInfo = Yii::$app->request->pathInfo;
$class = "";
if($pathInfo == 'name/index' || $pathInfo == 'surname/index' || $pathInfo == 'additional-information/index' || $pathInfo == 'user-agent/index') $class="active";

$classReport = "";
if($pathInfo == 'users/dashboard' || $pathInfo == 'logs/index' || $pathInfo == '') $classReport = "active";
?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                            <!-- <li <?php // ($pathInfo == 'users/dashboard' || $pathInfo == '' ? 'class="active"' : '')?> >
                                <?php // Html::a('<span class="fa fa-desktop"></span> Показатели', ['/users/dashboard'], []); ?>
                            </li> -->
                    
                     <li>
                        <?= Html::a('<span class="fa fa-list"></span> <span class="xn-text">Проекты</span>', ['/projects/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-cogs"></span> <span class="xn-text">Настройки</span>', ['/settings/index'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-users"></span> <span class="xn-text">Пользователи</span>', ['/users/index'], []); ?>
                    </li>
                    <!-- <li>
                        <?php // Html::a('<span class="glyphicon glyphicon-copyright-mark"></span> <span class="xn-text">Каналы</span>', ['/channels/index'], []); ?>
                    </li> -->
                    <!-- <li>
                        <?php // Html::a('<span class="fa fa-book"></span><span class="xn-text">Отчеты</span>', ['/reports/index'], []); ?>
                    </li> -->

                   <!--  <li class="xn-openable <?=$class?>"  >
                        <a href="#"><span class="fa fa-bar-chart-o"></span><span class="xn-text">Справочники</span></a>
                        <ul>
                        </ul>
                    </li> -->

                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->

