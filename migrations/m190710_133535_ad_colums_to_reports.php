<?php

use yii\db\Migration;

/**
 * Class m190710_133535_ad_colums_to_reports
 */
class m190710_133535_ad_colums_to_reports extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%reports}}', 'create_at', $this->dateTime()->comment('Дата добавления'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%reports}}', 'create_at');
    }
}
