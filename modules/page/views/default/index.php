<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;

?> 
<div class="row">
    <div class="col-md-12 ">
        <h1  class="company">  
            <span><?= $user->fio ?></span>
        </h1>
            <br><br>
            <div style="padding-left: 150px;">
                <?= $user->description ?>
            </div>
    </div>
</div>

<div class="row">
    <?php
        foreach ($questionary as $value) {
    ?> 
        <div class="col-md-3" style="margin-top: 10px;">
            <div class="panel panel-success top-non">
                <div class="panel panel-success">
                    <div class="panel-body profile">
                        <div class="top-info-profil">
                            <div class="top-info">
                                <p><b>Название</b><br><?= $value->name; ?></p>
                                <p><b>Ссылка на тест</b><br> <?= Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/' . $value->link , [ '/'.$value->link ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]); ?></p>
                            </div>
                        </div>
                    </div>                                
                    <div class="panel-body">
                        <div class="desc-info">
                            <?= $value->description; ?>
                        </div>
                    </div>                                
                </div>
            </div>
        </div>
  <?php } ?> 
</div>
