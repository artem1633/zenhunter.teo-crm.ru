<?php

use yii\db\Migration;

/**
 * Handles adding project_id to table `{{%reports}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%projects}}`
 */
class m190705_084541_add_project_id_column_to_reports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%reports}}', 'project_id', $this->integer()->comment('Отчеты'));

        // creates index for column `project_id`
        $this->createIndex(
            '{{%idx-reports-project_id}}',
            '{{%reports}}',
            'project_id'
        );

        // add foreign key for table `{{%projects}}`
        $this->addForeignKey(
            '{{%fk-reports-project_id}}',
            '{{%reports}}',
            'project_id',
            '{{%projects}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%projects}}`
        $this->dropForeignKey(
            '{{%fk-reports-project_id}}',
            '{{%reports}}'
        );

        // drops index for column `project_id`
        $this->dropIndex(
            '{{%idx-reports-project_id}}',
            '{{%reports}}'
        );

        $this->dropColumn('{{%reports}}', 'project_id');
    }
}
