<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="tenants-form">

    <?php $form = ActiveForm::begin(); ?>
  
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'fileUploading')->fileInput() ?>
        </div>
        <div class="col-md-4">
            <br>
            <?= Html::a('Скачать Шаблон  <i class="glyphicon glyphicon-download"></i>', ['export'], ['title'=> 'Добавить', 'class'=>'btn btn-success']);?>
                            </div>
    </div>
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
