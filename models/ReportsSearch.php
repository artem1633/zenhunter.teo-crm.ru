<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reports;

/**
 * ReportsSearch represents the model behind the search form about `app\models\Reports`.
 */
class ReportsSearch extends Reports
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'view_count', 'reading_count', 'status', 'project_id', 'channel_id'], 'integer'],
            [['type', 'link', 'title', 'date'], 'safe'],
            [['reading_prosent', 'average_time', 'like_count', 'comment_count'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = Reports::find()->where(['project_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'date' => $this->date,
            'view_count' => $this->view_count,
            'reading_count' => $this->reading_count,
            'reading_prosent' => $this->reading_prosent,
            'average_time' => $this->average_time,
            'like_count' => $this->like_count,
            'comment_count' => $this->comment_count,
            'status' => $this->status,
            'project_id' => $this->project_id,
            'channel_id' => $this->channel_id,
        ]);

        $query->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    public function searchSecond($params,$id)
    {
        $query = Reports::find()->where(['channel_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'date' => $this->date,
            'view_count' => $this->view_count,
            'reading_count' => $this->reading_count,
            'reading_prosent' => $this->reading_prosent,
            'average_time' => $this->average_time,
            'like_count' => $this->like_count,
            'comment_count' => $this->comment_count,
            'status' => $this->status,
            'project_id' => $this->project_id,
            'channel_id' => $this->channel_id,
        ]);

        $query->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
