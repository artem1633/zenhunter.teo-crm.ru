<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Channels */
?>
<div class="channels-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'link',
            //'name',
        ],
    ]) ?>

</div>
