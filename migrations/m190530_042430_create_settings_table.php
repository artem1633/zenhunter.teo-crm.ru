<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m190530_042430_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->comment('Наименование'),
            'key' => $this->string(255)->comment('Ключ'),
            'value' => $this->text()->comment('Значение'),
        ]);

        $this->insert('settings',array(
            'name' => 'Таймаут между обращением (число в мс)',
            'key' => 'time_out_appeals',
            'value' => '100', 
        ));

        $this->insert('settings',array(
            'name' => 'Использовать прокси',
            'key' => 'using_proxy',
            'value' => '1', 
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
