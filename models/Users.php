<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property string $telephone
 * @property int $type
 * @property string $telegram_id
 * @property double $main_balance Баланс основной
 * @property double $partner_balance Баланс партнерский
 * @property int $referal_id Реферал
 * @property double $prosent_referal Процент с оплат рефералов
 */

class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $new_password;

    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['fio', 'login', 'password', 'telephone', 'telegram_id', 'new_password'], 'string', 'max' => 255],
        ];
    } 

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'type' => 'Должность',
            'password' => 'Пароль',
            'telegram_id' => 'Id чат телеграма',
            'new_password' => 'Новый пароль',
            'telephone' => 'Телефон',
            'agree' => 'Я согласен с обработкой персональных данных',
            'prosent_referal' => 'Процент с оплат рефералов',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }

        if($this->new_password != null) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }       
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {        
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function getPermissionList()
    {
        return [
            0 => "Супер Администратор",
            1 => "Администратор",
            2 => "Менеджер",
        ];
    }

    public static function getAllInArray()
    {
        $companies = self::find()
            ->orderBy(['id' => SORT_DESC]);

        return ArrayHelper::map($companies->all(), 'id', 'fio');
    }

    public function getPermission()
    {
        if($this->type == 0) return "Супер Администратор";
        if($this->type == 1) return "Администратор";
        if($this->type == 2) return "Менеджер";
    }

    //Список тарифов.
    public function getTariffList()
    {
        $tariffs = Tariffs::find()->all();
        return ArrayHelper::map( $tariffs , 'id', 'name');
    }
}
