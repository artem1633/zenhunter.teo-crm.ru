<?php

use yii\db\Migration;

/**
 * Handles adding channel_id to table `{{%reports}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%reports}}`
 */
class m190705_111921_add_channel_id_column_to_reports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%reports}}', 'channel_id', $this->integer()->comment('Канал'));

        // creates index for column `channel_id`
        $this->createIndex(
            '{{%idx-reports-channel_id}}',
            '{{%reports}}',
            'channel_id'
        );

        // add foreign key for table `{{%channels}}`
        $this->addForeignKey(
            '{{%fk-reports-channel_id}}',
            '{{%reports}}',
            'channel_id',
            '{{%channels}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%channels}}`
        $this->dropForeignKey(
            '{{%fk-reports-channel_id}}',
            '{{%reports}}'
        );

        // drops index for column `channel_id`
        $this->dropIndex(
            '{{%idx-reports-channel_id}}',
            '{{%reports}}'
        );

        $this->dropColumn('{{%reports}}', 'channel_id');
    }
}
