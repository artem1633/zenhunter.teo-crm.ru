<?php

use app\models\Channels;
use app\models\Reports;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($data){
            $count = Reports::find()->where(['project_id' => $data->id])->count();
            return Html::a($data->name . ' / '. $count, ['/reports/index', 'id' => $data->id], ['data-pjax'=>'0','title'=> 'Отчеты',]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sub_category',
        'header' => 'Каналы',
        'width' => '100px',
        'content' => function($data){
            $count = Channels::find()->where(['project_id' => $data->id])->count();
            return '<center>' . Html::a('Каналы / '. $count, ['/channels/index', 'id' => $data->id], ['data-pjax'=>'0','title'=> 'Каналы', 'class'=>'btn btn-warning']) . '</center>';
        }
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sub_category',
        'header' => 'Отчеты',
        'width' => '100px',
        'content' => function($data){
            $count = Reports::find()->where(['project_id' => $data->id])->count();
            return '<center>' . Html::a('Отчеты / '. $count, ['/reports/index', 'id' => $data->id], ['data-pjax'=>'0','title'=> 'Отчеты', 'class'=>'btn btn-success']) . '</center>';
        }
    ],*/
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{leadUpdate} {leadDelete}',
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'buttons'  => [ 
            'leadProject' => function ($url, $model) {
                $url = Url::to(['/channels/index', 'id' => $model->id]);
                return Html::a('<span class="fa fa-tasks"></span>', $url, ['data-pjax'=>'0','title'=>'Проекты', 'data-toggle'=>'tooltip', 'target' => '_blank']);
            },
            'leadReport' => function ($url, $model) {
                $url = Url::to(['/reports/index', 'id' => $model->id]);
                return Html::a('<span class="fa fa-bar-chart-o"></span>', $url, ['data-pjax'=>'0','title'=>'Отчеты', 'data-toggle'=>'tooltip', 'target' => '_blank']);
            },  
            'leadUpdate' => function ($url, $model) {       
                $url = Url::to(['/projects/update', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/projects/delete', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемент?',
                ]);
            },
        ],
    ],

];   