<?php

use yii\helpers\Html;
use app\models\Settings;

?>      

<div class="test-container lightmode">        
    <div class="test-box animated fadeInDown">
        <div class="test-body" style="color: #fff !important;">
	        <?= Settings::find()->where(['key' => 'policy'])->one()->text ?>
    	</div>
	</div>
</div>