<?php

use yii\db\Migration;

/**
 * Class m190720_143849_addd_colums_to_report
 */
class m190720_143849_addd_colums_to_report extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%reports}}', 'update_at', $this->dateTime()->comment('Дата обновления'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%reports}}', 'update_at');
    }
}
