<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reports */
?>
<div class="reports-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
