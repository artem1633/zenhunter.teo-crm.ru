<?php

use yii\db\Migration;

/**
 * Class m190709_113908_update_colums_to_report
 */
class m190709_113908_update_colums_to_report extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%reports}}', 'average_time');
        $this->addColumn('{{%reports}}', 'average_time', $this->integer()->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'reading_prosent');
        $this->addColumn('{{%reports}}', 'reading_prosent', $this->integer()->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'reading_count');
        $this->addColumn('{{%reports}}', 'reading_count', $this->integer()->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'view_count');
        $this->addColumn('{{%reports}}', 'view_count', $this->integer()->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'date');
        $this->addColumn('{{%reports}}', 'date', $this->string(255)->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'like_count');
        $this->addColumn('{{%reports}}', 'like_count', $this->integer()->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'comment_count');
        $this->addColumn('{{%reports}}', 'comment_count', $this->integer()->comment('Тип'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%reports}}', 'average_time');
        $this->addColumn('{{%reports}}', 'average_time', $this->string(255));

        $this->dropColumn('{{%reports}}', 'reading_prosent');
        $this->addColumn('{{%reports}}', 'reading_prosent', $this->string(255));

        $this->dropColumn('{{%reports}}', 'reading_count');
        $this->addColumn('{{%reports}}', 'reading_count', $this->string(255));

        $this->dropColumn('{{%reports}}', 'date');
        $this->addColumn('{{%reports}}', 'date', $this->dateTime());

        $this->dropColumn('{{%reports}}', 'like_count');
        $this->addColumn('{{%reports}}', 'like_count', $this->string(255));

        $this->dropColumn('{{%reports}}', 'comment_count');
        $this->addColumn('{{%reports}}', 'comment_count', $this->string(255));

        $this->dropColumn('{{%reports}}', 'comment_count');
        $this->addColumn('{{%reports}}', 'comment_count', $this->string(255));
    }
}
