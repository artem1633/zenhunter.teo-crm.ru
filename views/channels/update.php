<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Channels */
?>
<div class="channels-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
