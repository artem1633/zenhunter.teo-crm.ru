<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\widgets\Bulk;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчеты';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);

?>
<div class="faq-index">
    <div id="ajaxCrudDatatable">
     <?=GridView::widget([
         'id'=>'crud-datatable',
         'dataProvider' => $dataProvider,
         'filterModel' => $searchModel,
         'pjax'=>true,
         'responsiveWrap' => false,
         'columns' => require(__DIR__.'/_columns.php'),
         'toolbar'=> [
         ['content'=>
                 '<div style="margin-top:10px;">' .
         Html::a('Назадь', ['/projects/index'],
         ['data-pjax'=>'0','title'=> 'Назадь', 'class'=>'btn btn-warning']).
         Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create', 'id' => $id],
         ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
         '<ul class="panel-controls">
             <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
             <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
             <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
         </ul>  '.
        '</div>'
                ],
                    ],          
        'striped' => true,
        'condensed' => true,
        'responsive' => true,          
        'panel' => [
        'type' => 'warning', 
        'heading' => '<i class="glyphicon glyphicon-list"></i> '. $name,
        'before'=>'',
        'after'=>BulkButtonWidget::widget([
        'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
            ["bulk-delete"] ,
            [
                "class"=>"btn btn-danger btn-xs",
                'role'=>'modal-remote-bulk',
                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                'data-request-method'=>'post',
                'data-confirm-title'=>'Подтвердите действие',
                'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
            ]),
        ]).Bulk::widget([
            'buttons'=>Html::a('<i class="glyphicon glyphicon-pencil"></i>&nbsp; Cменить статус',
                ["change-all"/*, 'application_id' => $application->id*/ ] ,
                [
                    "class"=>"btn btn-info btn-xs",
                    'role'=>'modal-remote-bulk',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены чтобы изменить этих выделенных элементов?'
                ]),
        ]).
        '<div class="clearfix"></div>',
                    ]
                ])?>
            </div>
        </div>
        <?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
