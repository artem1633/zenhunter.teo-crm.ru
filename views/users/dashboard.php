<?php

use app\models\Lessons;
use app\models\Settings;
use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;
use app\models\Resume;
use app\models\ResumeStatus; 
use app\models\LessonsUsers;
use yii\bootstrap\Modal;
$this->title = 'Рабочий стол';
?>
<div class="site-index">
    <button  type="button" class="btn btn-xs btn-warning" onclick='startIntro();'>Начать обучение</button>
    <div class="row">
        <div class="col-md-3 col-xs-12 col-sm-6 introduction-first" data-introindex="13" >
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-file"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$questionaryCount?></div>
                    <div class="widget-title">Количество тестов</div>
                    <!-- <div class="widget-subtitle">That visited our site today</div> -->
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6 introduction-first" data-introindex="14" >
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-files-o"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$resumeCount?></div>
                    <div class="widget-title">Количество результатов</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6 introduction-first" data-introindex="15" >
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-envelope"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$resumeTodayCount?></div>
                    <div class="widget-title">Количество новых результатов сегодня</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-xs-12 col-sm-6 introduction-first" data-introindex="16" >
            <div class="widget widget-warning widget-item-icon">
                <div class="widget-item-left">
                    <span class="fa fa-send"></span>
                </div>
                <div class="widget-data">
                    <div class="widget-int num-count"><?=$sendCount?></div>
                    <div class="widget-title">Подключен к телеграм</div>
                </div>
                <div class="widget-controls">                                
                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Удалить виджет"><span class="fa fa-times"></span></a>
                </div>
            </div>
        </div>

    </div>
    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success panel-hidden-controls ">
                <div class="panel-heading ui-draggable-handle">
                    <h3 class="panel-title">Информация за месяц количество результатов</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                            </ul>                                        
                        </li>
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>                                
                </div>
                <div class="panel-body introduction-first" data-introindex="17" >
                    
                    <div class="col-md-12">
                        <?= ChartJs::widget([
                                'type' => 'line',
                                'options' => [
                                    'height' => 80,
                                    'width' => 400
                                ],
                                'data' => [
                                    'labels' => ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                                    'datasets' => [
                                        [
                                            'label' => "Договоры",
                                            'backgroundColor' => "rgba(179,181,198,0.2)",
                                            'borderColor' => "rgba(179,181,198,1)",
                                            'pointBackgroundColor' => "rgba(179,181,198,1)",
                                            'pointBorderColor' => "#fff",
                                            'pointHoverBackgroundColor' => "#fff",
                                            'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                            'data' => [65, 59, 90, 81, 56, 55, 40, 12, 25, 32, 30, 42]
                                        ],
                                       
                                    ]
                                ]
                            ]);
                            ?>
                    </div>

                </div>      
                <div class="panel-footer">
                </div>                            
            </div>
        </div>
    </div>
</div>