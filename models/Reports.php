<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "reports".
 *
 * @property int $id
 * @property string $link URL
 * @property string $update_at URL
 * @property int $type тип
 * @property string $title Заголовок
 * @property string $date Дата публикации
 * @property string $create_at Дата публикации
 * @property int $view_count Количество просмотров
 * @property int $reading_count Количество дочитываний
 * @property double $reading_prosent Процент дочитываний 
 * @property double $average_time Среднее время дочитывания
 * @property double $like_count Количество лайков
 * @property double $comment_count Количество комментариев
 * @property int $status Статус
 * @property int $project_id Отчеты
 * @property int $channel_id Канал
 *
 * @property Channels $channel
 * @property Projects $project
 */
class Reports extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reports';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link'], 'unique'],
            [['status', 'project_id', 'channel_id'], 'integer'],
            [['date','create_at','update_at'], 'safe'],
            [['reading_prosent', 'average_time', 'like_count', 'comment_count'], 'number'],
            [[ 'link', 'title','view_count', 'reading_count', ], 'string', 'max' => 255],
            [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Channels::className(), 'targetAttribute' => ['channel_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'URL',
            'type' => 'Тип',
            'title' => 'Заголовок',
            'date' => 'Дата публикации',
            'view_count' => 'Просмотров',
            'reading_count' => 'Дочитываний',
            'reading_prosent' => 'Процент дочитываний ',
            'average_time' => 'Время дочитывания',
            'like_count' => 'Лайков',
            'comment_count' => 'Комментариев',
            'status' => 'Статус',
            'project_id' => 'Отчеты',
            'channel_id' => 'Канал',
            'create_at' => 'Дата добавления',
            'update_at' => 'Дата update',
        ];
    }

    
    public function getType()
    {
        return [
            1 => 'Нарратив',
            2 => 'Статья',
        ];
    }
    public function getTypeDescription()
    {
        switch ($this->type) {
            case 1: return "Нарратив";
            case 2: return "Статья";
            default: return "Неизвестно";
        }
    }
    
    public function getChannelsList()
    {
        $channel = Channels::find()->where(['project_id' => $this->project_id])->all();
        return ArrayHelper::map($channel,'id','name');
    }

    public function getStatus()
    {
        return [
            1 => 'Свободно',
            2 => 'Запланирован',
            3 => 'Использован',
        ];
    }
    public function getStatusDescription()
    {
        switch ($this->status) {
            case 1: return "Свободно";
            case 2: return "Запланирован";
            case 3: return "Использован";
            default: return "Неизвестно";
        }
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channels::className(), ['id' => 'channel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }
}
