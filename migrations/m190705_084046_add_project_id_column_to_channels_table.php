<?php

use yii\db\Migration;

/**
 * Handles adding project_id to table `{{%channels}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%projects}}`
 */
class m190705_084046_add_project_id_column_to_channels_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%channels}}', 'project_id', $this->integer()->comment('Проект'));

        // creates index for column `project_id`
        $this->createIndex(
            '{{%idx-channels-project_id}}',
            '{{%channels}}',
            'project_id'
        );

        // add foreign key for table `{{%projects}}`
        $this->addForeignKey(
            '{{%fk-channels-project_id}}',
            '{{%channels}}',
            'project_id',
            '{{%projects}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%projects}}`
        $this->dropForeignKey(
            '{{%fk-channels-project_id}}',
            '{{%channels}}'
        );

        // drops index for column `project_id`
        $this->dropIndex(
            '{{%idx-channels-project_id}}',
            '{{%channels}}'
        );

        $this->dropColumn('{{%channels}}', 'project_id');
    }
}
