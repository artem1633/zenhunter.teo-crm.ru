<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Reports;
use dosamigos\datepicker\DatePicker;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title',
        'content' => function($data){
            return Html::a($data->title, $data->link, ['data-pjax'=>'0', 'target' => '_blank', 'title'=> 'Войти',]);
        }
    ],
    /*[
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'link',
    ],*/
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'filter' => Reports::getType(),
        'content' => function($data){
            return $data->getTypeDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'create_at',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'view_count',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'reading_count',
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'reading_prosent',
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'average_time',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'like_count',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'comment_count',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'update_at',
     ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
         'filter' => Reports::getStatus(),
        'content' => function($data){
            return $data->getStatusDescription();
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'], 
    ],

];   