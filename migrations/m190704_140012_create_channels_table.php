<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%channels}}`.
 */
class m190704_140012_create_channels_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%channels}}', [
            'id' => $this->primaryKey(),
            'link' => $this->string(255)->comment('Ссылка'),
            'name' => $this->string(255)->comment('Название'),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%channels}}');
    }
}
