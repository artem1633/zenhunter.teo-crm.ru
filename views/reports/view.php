<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Reports */
?>
<div class="reports-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'link',
            'type',
            //'title',
            'date',
            'view_count',
            'reading_count',
            'reading_prosent',
            'average_time',
            'like_count',
            'comment_count',
            'status',
        ],
    ]) ?>

</div>
