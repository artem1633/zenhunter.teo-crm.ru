<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "channels".
 *
 * @property int $id
 * @property string $link Ссылка
 * @property string $name Название
 * @property int $project_id Проект
 *
 * @property Projects $project
 * @property Reports[] $reports
 */
class Channels extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'channels';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id'], 'integer'],
            [['link', 'name'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Projects::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Ссылка',
            'name' => 'Название',
            'project_id' => 'Проект',
        ];
    }

    public function beforeDelete()
    {
        $reports = Reports::find()->where(['channel_id' => $this->id])->all();
        foreach ($reports as $value) {
            $value->delete();
        }
        
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports()
    {
        return $this->hasMany(Reports::className(), ['channel_id' => 'id']);
    }
}
