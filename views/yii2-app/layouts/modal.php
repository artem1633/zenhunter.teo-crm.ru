<?php

use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<?= Html::csrfMetaTags() ?>
<?php $this->head() ?>
<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>