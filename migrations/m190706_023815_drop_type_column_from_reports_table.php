<?php

use yii\db\Migration;

/**
 * Handles dropping type from table `{{%reports}}`.
 */
class m190706_023815_drop_type_column_from_reports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%reports}}', 'average_time');
        $this->addColumn('{{%reports}}', 'average_time', $this->string(255)->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'reading_prosent');
        $this->addColumn('{{%reports}}', 'reading_prosent', $this->string(255)->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'reading_count');
        $this->addColumn('{{%reports}}', 'reading_count', $this->string(255)->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'view_count');
        $this->addColumn('{{%reports}}', 'view_count', $this->string(255)->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'date');
        $this->addColumn('{{%reports}}', 'date', $this->string(255)->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'like_count');
        $this->addColumn('{{%reports}}', 'like_count', $this->string(255)->comment('Тип'));

        $this->dropColumn('{{%reports}}', 'comment_count');
        $this->addColumn('{{%reports}}', 'comment_count', $this->string(255)->comment('Тип'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%reports}}', 'average_time');
        $this->addColumn('{{%reports}}', 'average_time', $this->integer());

        $this->dropColumn('{{%reports}}', 'reading_prosent');
        $this->addColumn('{{%reports}}', 'reading_prosent', $this->integer());

        $this->dropColumn('{{%reports}}', 'reading_count');
        $this->addColumn('{{%reports}}', 'reading_count', $this->integer());

        $this->dropColumn('{{%reports}}', 'date');
        $this->addColumn('{{%reports}}', 'date', $this->dateTime());

        $this->dropColumn('{{%reports}}', 'like_count');
        $this->addColumn('{{%reports}}', 'like_count', $this->integer());

        $this->dropColumn('{{%reports}}', 'comment_count');
        $this->addColumn('{{%reports}}', 'comment_count', $this->integer());

        $this->dropColumn('{{%reports}}', 'comment_count');
        $this->addColumn('{{%reports}}', 'comment_count', $this->integer());
    }
}
