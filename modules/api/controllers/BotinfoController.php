<?php

namespace app\modules\api\controllers;

use app\models\Alert;
use app\models\Chat;
use app\models\JobTable;
use app\models\Langs;
use app\models\Languages;
use app\models\Questionary;
use app\models\ReferalRedirects;
use app\models\Resume;
use app\models\Settings;
use app\models\User;
use app\models\Users;
use app\models\UsersList;
use app\models\Words;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class BotinfoController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','testpush','bot-update','send-refer'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма


        // if ($result["callback_query"]) {
        //     $this->botCall($result);
        //     return true;
        // }

        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
        $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя

        // $file = file_put_contents('income.txt', $content); // создаем текстовый файл для отладки(по желанию)

        $user_push = ['247187885'];//,'541366830'
        /** @var UsersList $user */
        $message_id = null;
        if ($result["callback_query"]) {
            $text = $result["callback_query"]['data'];
            $chat_id = $result['callback_query']['message']['chat']['id'];
            $message_id = $result['callback_query']['message']['message_id'];
//            self::sendTelMessage('247187885', "Ошибка создания {$text} - {$message_id} - {$chat_id}");
        }

        $user = UsersList::find()->where(['telegram_id' => $chat_id])->one();



        if ($user) {
            /** @var Chat $chat */
            $chat = Chat::find()->where([])->orderBy(['id' => SORT_DESC])->one();
        }



        if ($text == '/start') {
            if (!$user) {
                $user = new UsersList([
                    'name' => $name,
                    'telegram_id' => $chat_id,
                    'balance' => 0,
                    'partner_code' => self::genCode(),
                    'end_date' => date('Y-d-m','+5 day'),
                    'status_pay' => 0,
                ]);

                if (!$user->save(false)){
                    $a = serialize($user->errors);
                    self::sendTelMessage('247187885', "Ошибка создания {$a}");
                    return true;
                }
                $langAll = Langs::find()->all();
                foreach ($langAll as $item) {
                    $lan[] = ["text" => $item->name,'callback_data' => $item->id];
                }
//
                $this->getReq('sendMessage',[
                    'chat_id'=> $chat_id,
                    'text'=>"{$name}, выберите язык:",
                    'reply_markup' => json_encode([
                        'inline_keyboard' =>  [$lan],
                    ])
                ]);

                self::newStep($user, 'step1', 0);
                return true;
            } else {
                $this->newMessage($chat_id, self::getWord(101, $user->language_id, $user));
                return true;
            }
        }

        $this->setClear($chat_id, $message_id);

        if ($text == 'ref_key') {
            $info =  self::getWord(101, $user->language_id, $user);
        }
        if ($text == 'end_date_key') {
            $info =  self::getWord(100, $user->language_id, $user);
        }


        if ($text != '/start') {
            if ($chat->text == 'step1') {
                $this->step1($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step2') {
                $this->step2($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step4') {
                $this->step4($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step6') {
                $this->step6($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step8') {
                $this->step8($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step9') {
                $this->step9($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step10') {
                $this->step10($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step13') {
                $this->step13($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step14') {
                $this->step14($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step15') {
                $this->step15($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step16') {
                $this->step16($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step17') {
                $this->step17($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step18') {
                $this->step18($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step19') {
                $this->step19($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step22') {
                $photo = $result["message"]["photo"]; //Юзернейм пользователя
                $this->step22($user, $chat_id, $message_id, $text, $photo);
                return true;
            }
            if ($chat->text == 'step23') {
                $this->step23($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step24') {
                $this->step24($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step26') {
                $this->step26($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step27') {
                $this->step27($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step29') {
                $this->step29($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step30') {
                $this->step30($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step31') {
                $this->step31($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step32') {
                $this->step32($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step33') {
                $this->step33($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step34') {
                $this->step34($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step36') {
                $this->step36($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step37') {
                $this->step37($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step38') {
                $this->step38($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step39') {
                $this->step39($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step40') {
                $this->step40($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step42') {
                $this->step42($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step43') {
                $this->step43($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step44') {
                $this->step44($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step45') {
                $this->step45($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step46') {
                $this->step46($user, $chat_id, $message_id, $text);
                return true;
            }
            if ($chat->text == 'step47') {
                if (!$info) {
                    $info = self::getWord(99, $user->language_id, $user);
                }
                $this->getReq('sendMessage',[
                    'chat_id'=> $chat_id,
                    'text'=> $info,
                    'reply_markup' => json_encode([
                        'inline_keyboard' =>  [
                            [
                                ['text' => self::getWord(91, $user->language_id, $user),'callback_data' => 'ref_key'],
                                ['text' => self::getWord(92, $user->language_id, $user),'callback_data' => 'balans_key'],
                                ['text' => self::getWord(93, $user->language_id, $user),'callback_data' => 'end_date_key'],
                                ['text' => self::getWord(94, $user->language_id, $user),'callback_data' => 'pay_key'],
                            ],[
                                ['text' => self::getWord(95, $user->language_id, $user),'callback_data' => 'add_work_key'],
                                ['text' => self::getWord(96, $user->language_id, $user),'callback_data' => 'up_wall_key'],
                                ['text' => self::getWord(97, $user->language_id, $user),'callback_data' => 'up_pogoda_key'],
                            ]],
                        ])
                    ]);
                return true;
            }
            return true;
        }

        return true;

//

    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step45($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {

            $this->newMessage($chat_id, self::getWord(87, $user->language_id, $user));

            self::newStep($user, 'step44', 0);

            $this->setClear($chat_id, $message_id);

            return true;
        }

        if ($text == '+') {


            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(98, $user->language_id, $user));

            self::newStep($user, 'step46', 0);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> self::getWord(90, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(91, $user->language_id, $user),'callback_data' => 'ref_key'],
                        ['text' => self::getWord(92, $user->language_id, $user),'callback_data' => 'balans_key'],
                        ['text' => self::getWord(93, $user->language_id, $user),'callback_data' => 'end_date_key'],
                        ['text' => self::getWord(94, $user->language_id, $user),'callback_data' => 'pay_key'],
                        ['text' => self::getWord(95, $user->language_id, $user),'callback_data' => 'add_work_key'],
                        ['text' => self::getWord(96, $user->language_id, $user),'callback_data' => 'up_wall_key'],
                        ['text' => self::getWord(97, $user->language_id, $user),'callback_data' => 'up_pogoda_key'],
                    ]],
                ])
            ]);

            self::newStep($user, 'step47', 0);

            return true;

        }

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step44($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) < 5) {
            $this->newMessage($chat_id, self::getWord(83, $user->language_id, $user));
            return true;
        }

        $this->getApply($chat_id, self::getWord(89, $user->language_id, $user));

        self::newStep($user, 'step45', 0);

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step43($user, $chat_id, $message_id, $text)
    {
        if ($text == '+') {

            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(87, $user->language_id, $user));

            self::newStep($user, 'step44', 0);

            $this->setClear($chat_id, $message_id);

            return true;

        }

        return true;
    }

    /**
     * @var UsersList  $user
     *
     */
    private  function step42($user, $chat_id, $message_id, $text)
    {
        if ($text == '+') {

            self::newStep($user, $text, 1);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> self::getWord(88, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(69, $user->language_id, $user),'callback_data' => '+']]],
                ])
            ]);

            self::newStep($user, 'step43', 0);

            return true;

        }

        return true;
    }




    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step40($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {
            $this->newMessage($chat_id, self::getWord(87, $user->language_id, $user));
            self::newStep($user, 'step36', 0);
            $this->setClear($chat_id, $message_id);
            return true;
        }

        if ($text == '+') {

            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(86, $user->language_id, $user));

            self::newStep($user, 'step41', 0);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>self::getWord(85, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(67, $user->language_id, $user),'callback_data' => '+']]],
                ])
            ]);

            self::newStep($user, 'step42', 0);

            return true;

        }

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step39($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) < 5) {
            $this->newMessage($chat_id, self::getWord(76, $user->language_id, $user));
            return true;
        }

        $this->getApply($chat_id, self::getWord(84, $user->language_id, $user));

        self::newStep($user, 'step40', 0);

        return true;
    }


    /**
     * @var UsersList  $user
     *
     */
    private  function step38($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) < 5) {
            $this->newMessage($chat_id, self::getWord(83, $user->language_id, $user));
            return true;
        }

        self::newStep($user, $text, 1);

        $this->newMessage($chat_id, self::getWord(82, $user->language_id, $user));

        self::newStep($user, 'step39', 0);

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step37($user, $chat_id, $message_id, $text)
    {
        if ($text == '+') {

            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(81, $user->language_id, $user));

            self::newStep($user, 'step38', 0);

            $this->setClear($chat_id, $message_id);

            return true;

        }

        return true;
    }

    /**
     * @var UsersList  $user
     *
     */
    private  function step36($user, $chat_id, $message_id, $text)
    {
        if ($text == '+') {

            self::newStep($user, $text, 1);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> self::getWord(80, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(69, $user->language_id, $user),'callback_data' => '+']]],
                ])
            ]);

            self::newStep($user, 'step37', 0);

            return true;

        }

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step34($user, $chat_id, $message_id, $text)
    {

        $job = JobTable::find()->where(['user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();

        if ($text == '-') {
            $job->delete();

            $this->newMessage($chat_id, self::getWord(79, $user->language_id, $user));

            self::newStep($user, 'step31', 0);

            $this->setClear($chat_id, $message_id);

            return true;
        }

        if ($text == '+') {

            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(78, $user->language_id, $user));

            self::newStep($user, 'step35', 0);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>self::getWord(77, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(67, $user->language_id, $user),'callback_data' => '+']]],
                ])
            ]);

            self::newStep($user, 'step36', 0);

            return true;

        }

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step33($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) < 5) {
            $this->newMessage($chat_id, self::getWord(76, $user->language_id, $user));
            return true;
        }

        $job = JobTable::find()->where(['user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();
        $job->time = $text;
        $job->save();


        $this->getApply($chat_id, self::getWord(75, $user->language_id, $user));

        self::newStep($user, 'step34', 0);

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step32($user, $chat_id, $message_id, $text)
    {
        $job = JobTable::find()->where(['user_id' => $user->id])->orderBy(['id' => SORT_DESC])->one();

        if ($job) {
            $job->value = $text;
            $job->save(false);

            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(74, $user->language_id, $user));
            self::newStep($user, 'step33', 0);

            return true;

        }

        return true;
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step31($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) < 10) {
            $this->newMessage($chat_id, self::getWord(73, $user->language_id, $user));
            return true;
        }

        $job = new JobTable([
            'user_id' => $user->id,
            'date' => date('Y-m-d', strtotime($text)),
        ]);
        if (!$job->save(false)){
            $a = serialize($job->errors);
            self::sendTelMessage('247187885', "Ошибка создания {$a}");
            return true;
        }


        self::newStep($user, $text, 1);

        $this->newMessage($chat_id, self::getWord(72, $user->language_id, $user));

        self::newStep($user, 'step32', 0);

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step30($user, $chat_id, $message_id, $text)
    {
        if ($text == '+') {


            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(71, $user->language_id, $user));

            self::newStep($user, 'step31', 0);


            $this->setClear($chat_id, $message_id);

            return true;

        }

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step29($user, $chat_id, $message_id, $text)
    {
        if ($text == '+') {


            self::newStep($user, $text, 1);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> self::getWord(70, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(69, $user->language_id, $user),'callback_data' => '+']]],
                ])
            ]);

            self::newStep($user, 'step30', 0);

            return true;

        }

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step27($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {

            $this->newMessage($chat_id, self::getWord(59, $user->language_id, $user));
            self::newStep($user, 'step26', 0);

            return true;
        }

        if ($text == '+') {


            self::newStep($user, $text, 1);

            $p = '';
            if ($user->bot_gender == 0) {
                $p = 'Женский';
            }

            if ($user->bot_gender == 1) {
                $p = 'Мужской';
            }

            $this->newMessage($chat_id, self::getWord(68, $user->language_id, $user));

            self::newStep($user, 'step28', 0);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>self::getWord(53, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(67, $user->language_id, $user),'callback_data' => '+']]],
                ])
            ]);

            self::newStep($user, 'step29', 0);

            return true;

        }

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step26($user, $chat_id, $message_id, $text)
    {
        $p = '';
        if ($text == '2') {
            $p = self::getWord(41, $user->language_id, $user);
            $user->bot_gender = 0;
            $user->save(false);
        }

        if ($text == '1') {
            $p = self::getWord(40, $user->language_id, $user);
            $user->bot_gender = 1;
            $user->save(false);

        }

        self::newStep($user, $text, 1);

        $this->setClear($chat_id, $message_id);

        $this->getApply($chat_id,self::getWord(51, $user->language_id, $user));

        self::newStep($user, 'step27', 0);

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step24($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {
            $this->newMessage($chat_id, self::getWord(48, $user->language_id, $user));
            self::newStep($user, 'step23', 0);
            return true;
        }

        if ($text == '+') {

            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(49, $user->language_id, $user));

            self::newStep($user, 'step25', 0);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> self::getWord(50, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(40, $user->language_id, $user),'callback_data' => '1'],['text' => self::getWord(41, $user->language_id, $user),'callback_data' => '2']]],
                ])
            ]);

            self::newStep($user, 'step26', 0);

            return true;

        }

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step23($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) < 3) {
            $this->newMessage($chat_id, self::getWord(66, $user->language_id, $user));
            return true;
        }

        $user->bot_name = $text;
        $user->save(false);

        self::newStep($user, $text, 1);

        $this->getApply($chat_id,self::getWord(65, $user->language_id, $user));

        self::newStep($user, 'step24', 0);

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step22($user, $chat_id, $message_id, $text, $photo)
    {
        if (!$photo) {
            $this->newMessage($chat_id, self::getWord(45, $user->language_id, $user));
            return true;
        }

        $jsonResponse = $this->getReq('getFile',[
            'file_id'=> $photo[0]['file_id'],
        ],1);

        $file_path = $jsonResponse['result']['file_path'];


        $user->avatar_bot = 'https://api.telegram.org/file/bot708108180:AAHrXLS0HuyyCVyUhCO4fZk0iHn5ArXAPI4/'.$file_path;
        $user->save(false);

        $this->newMessage($chat_id, self::getWord(46, $user->language_id, $user));

        self::newStep($user, 'step23', 0);

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step19($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {

            $this->newMessage($chat_id, self::getWord(59, $user->language_id, $user));

            self::newStep($user, 'step18', 0);

            return true;
        }

        if ($text == '+') {

            self::newStep($user, $text, 1);

            $p = '';
            if ($user->gender == 0) {
                $p = self::getWord(41, $user->language_id, $user);
            }

            if ($user->gender == 1) {
                $p = self::getWord(40, $user->language_id, $user);
            }

            $this->newMessage($chat_id, self::getWord(64, $user->language_id, $user));

            $this->setClear($chat_id, $message_id);

            self::newStep($user, 'step20', 0);

            $this->newMessage($chat_id, self::getWord(43, $user->language_id, $user));

            self::newStep($user, 'step21', 0);

            $this->newMessage($chat_id, self::getWord(44, $user->language_id, $user));

            self::newStep($user, 'step22', 0);

            return true;

        }

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step18($user, $chat_id, $message_id, $text)
    {
        $p = '';
        if ($text == '2') {
            $p = self::getWord(41, $user->language_id, $user);
            $user->gender = 0;
            $user->save(false);
        }

        if ($text == '1') {
            $p = self::getWord(40, $user->language_id, $user);
            $user->gender = 1;
            $user->save(false);

        }

        self::newStep($user, $text, 1);

        $this->setClear($chat_id, $message_id);

        $this->getApply($chat_id,self::getWord(42, $user->language_id, $user));

        self::newStep($user, 'step19', 0);

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step17($user, $chat_id, $message_id, $text)
    {
        if ($text == '2') {

            $this->setClear($chat_id, $message_id);

            $this->newMessage($chat_id, self::getWord(32, $user->language_id, $user));

            self::newStep($user, 'step17', 0);

            return true;
        }

        if ($text == '1') {

            self::newStep($user, $text, 1);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=> self::getWord(39, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(40, $user->language_id, $user),'callback_data' => '1'],['text' => self::getWord(41, $user->language_id, $user),'callback_data' => '2']]],
                ])
            ]);

            self::newStep($user, 'step18', 0);

            return true;

        }

        return true;
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step16($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) > 5 or strlen($text) < 5) {
            $this->newMessage($chat_id, self::getWord(63, $user->language_id, $user));
            return true;
        }

        $user->time_for_course = $text;
        $user->save(false);


        self::newStep($user, $text, 1);

        $this->newMessage($chat_id, self::getWord(62, $user->language_id, $user));

        $this->newMessage($chat_id, self::getWord(61, $user->language_id, $user));

        $this->getReq('sendMessage',[
            'chat_id'=> $chat_id,
            'text'=>self::getWord(60, $user->language_id, $user),
            'reply_markup' => json_encode([
                'inline_keyboard' =>  [[['text' => self::getWord(37, $user->language_id, $user),'callback_data' => '1'],['text' => self::getWord(38, $user->language_id, $user),'callback_data' => '2']]],
            ])
        ]);

        self::newStep($user, 'step17', 0);

        return true;
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step15($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {

            $this->setClear($chat_id, $message_id);

            $this->newMessage($chat_id, self::getWord(59, $user->language_id, $user));

            $user->currency_course = '';
            $user->save(false);

            self::newStep($user, 'step14', 0);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>self::getWord(54, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[
                        [
                            'text' => 'Доллар',
                            'callback_data' => 'Доллар'
                        ],
                        [
                            'text' => 'Евро',
                            'callback_data' => 'Евро'
                        ],
                        [
                            'text' => 'Рубли',
                            'callback_data' => 'Рубли'
                        ],
                        [
                            'text' => 'Тенге',
                            'callback_data' => 'Тенге'
                        ],
                        [
                            'text' => self::getWord(55, $user->language_id, $user),
                            'callback_data' => '5'
                        ],
                    ]],
                ])
            ]);

            return true;
        }

        if ($text == '+') {

            $this->setClear($chat_id, $message_id);


            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(35, $user->language_id, $user));

            self::newStep($user, 'step16', 0);

            return true;

        }

        return true;
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step14($user, $chat_id, $message_id, $text)
    {
        if ($text != 5) {
            $user->currency_course = $user->currency_course . $text . ',';
            $user->save(false);

            $this->newMessage($chat_id, self::getWord(58, $user->language_id, $user));
            return true;
        }

        self::newStep($user, $text, 1);

        $this->setClear($chat_id, $message_id);

        $this->getApply($chat_id,self::getWord(57, $user->language_id, $user));

        self::newStep($user, 'step15', 0);

        return true;


        return true;
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step13($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>self::getWord(56, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(37, $user->language_id, $user),'callback_data' => '1'],['text' => self::getWord(38, $user->language_id, $user),'callback_data' => '2']]],
                ])
            ]);

            self::newStep($user, 'step17', 0);

            return true;
        }

        if ($text == '+') {


            self::newStep($user, $text, 1);

            $this->setClear($chat_id, $message_id);

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>self::getWord(54, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[
                        [
                            'text' => 'Доллар',
                            'callback_data' => 'Доллар'
                        ],
                        [
                            'text' => 'Евро',
                            'callback_data' => 'Евро'
                        ],
                        [
                            'text' => 'Рубли',
                            'callback_data' => 'Рубли'
                        ],
                        [
                            'text' => 'Тенге',
                            'callback_data' => 'Тенге'
                        ],
                        [
                            'text' => self::getWord(55, $user->language_id, $user),
                            'callback_data' => '5'
                        ],
                    ]],
                ])
            ]);

            self::newStep($user, 'step14', 0);

            return true;

        }

        return true;
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step10($user, $chat_id, $message_id, $text)
    {
        if ($text == '2') {

            $this->setClear($chat_id, $message_id);

            $this->newMessage($chat_id, self::getWord(32, $user->language_id, $user));

            self::newStep($user, 'step11', 0);

            return true;
        }

        if ($text == '1') {


            self::newStep($user, $text, 1);

            $this->setClear($chat_id, $message_id);

            $this->newMessage($chat_id, self::getWord(30, $user->language_id, $user));

            self::newStep($user, 'step12', 0);

            $this->getApply($chat_id,self::getWord(31, $user->language_id, $user));

            self::newStep($user, 'step13', 0);

            return true;

        }

        return true;
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step9($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) > 5 or strlen($text) < 5) {
            $this->newMessage($chat_id, self::getWord(27, $user->language_id, $user));

            return true;
        }


        $this->setClear($chat_id, $message_id);

        $user->time_for_weather = $text;
        $user->save(false);


        self::newStep($user, $text, 1);

        $this->newMessage($chat_id, self::getWord(28, $user->language_id, $user)." {$text}");

        $this->getReq('sendMessage',[
            'chat_id'=> $chat_id,
            'text'=>self::getWord(24, $user->language_id, $user),
            'reply_markup' => json_encode([
                'inline_keyboard' =>  [[['text' => self::getWord(37, $user->language_id, $user),'callback_data' => '1'],['text' => self::getWord(38, $user->language_id, $user),'callback_data' => '2']]],
            ])
        ]);

        self::newStep($user, 'step10', 0);

        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step8($user, $chat_id, $message_id, $text)
    {
        if ($text == '-') {


            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>self::getWord(24, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(37, $user->language_id, $user),'callback_data' => '1'],['text' => self::getWord(38, $user->language_id, $user),'callback_data' => '2']]],
                ])
            ]);

            self::newStep($user, 'step10', 0);

            return true;
        }

        if ($text == '+') {

            $this->setClear($chat_id, $message_id);

            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(25, $user->language_id, $user));
            self::newStep($user, 'step9', 0);

            return true;

        }

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step6($user, $chat_id, $message_id, $text)
    {
        if (strlen($text) > 2) {
            $this->newMessage($chat_id, self::getWord(18, $user->language_id, $user));
            return true;
        }
        if ($text != '+' and $text != '-') {

            $a1 = date('H');
            $t = (int)$text - $a1 ;
            $user->timezone_for_moscow = $t;
            $user->save();


            $this->getApply($chat_id,self::getWord(19, $user->language_id, $user)."  {$t}");

            return true;
        }
        if ($text == '-') {
            $this->newMessage($chat_id, self::getWord(20, $user->language_id, $user));
        }

        if ($text == '+') {


            self::newStep($user, $text, 1);

            $this->setClear($chat_id, $message_id);

            $this->newMessage($chat_id, self::getWord(21, $user->language_id, $user));

            self::newStep($user, 'step7', 0);

            $this->newMessage($chat_id, self::getWord(22, $user->language_id, $user));


            self::newStep($user, 'step8', 0);


            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>self::getWord(23, $user->language_id, $user),
                'reply_markup' => json_encode([
                    'inline_keyboard' =>  [[['text' => self::getWord(33, $user->language_id, $user),'callback_data' => '+'],['text' => self::getWord(34, $user->language_id, $user),'callback_data' => '-']]],
                ])
            ]);

            return true;

        }

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step4($user, $chat_id, $message_id, $text)
    {
        if ($text != '+' and $text != '-') {

            $user->name = $text;
            $user->save();
            $this->getApply($chat_id,self::getWord(19, $user->language_id, $user).' '.$text);
            return true;
        }

        if ($text == '-') {

            $this->newMessage($chat_id, self::getWord(20, $user->language_id, $user));
        }

        if ($text == '+') {


            self::newStep($user, $text, 1);

            $this->setClear($chat_id, $message_id);

            $this->newMessage($chat_id, self::getWord(15, $user->language_id, $user));

            self::newStep($user, 'step5', 0);

            $this->newMessage($chat_id, self::getWord(16, $user->language_id, $user));


            self::newStep($user, 'step6', 0);

            $this->newMessage($chat_id, self::getWord(17, $user->language_id, $user));
            return true;

        }

        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step2($user, $chat_id, $message_id, $text)
    {
        /** @var UsersList $lang */
        $lang = UsersList::find()->where(['partner_code' => $text])->one();
        if (!$lang) {
            $this->newMessage($chat_id, self::getWord(7, $user->language_id, $user));
            return true;
        } else {

            $user->who_invited = (string)$lang->id;
            $user->save();


            self::newStep($user, $text, 1);

            $this->newMessage($chat_id, self::getWord(8, $user->language_id, $user));

        }

        self::newStep($user, 'step3', 0);

        $this->newMessage($chat_id, self::getWord(9, $user->language_id, $user));

        self::newStep($user, 'step4', 1);

        $this->newMessage($chat_id, self::getWord(3, $user->language_id, $user));
        return true;
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var integer $chat_id
     * @var integer $message_id
     * @var string $message_id
     * @return boolean
     */
    private  function step1($user, $chat_id, $message_id, $text)
    {
        $lang = Langs::find()->where(['id' => $text])->one();


        $user->language_id = $lang->id;
        $user->save();

        self::newStep($user, $text, 1);

        $this->newMessage($chat_id, self::getWord(5, $user->language_id, $user));

        $this->setClear($chat_id, $message_id);

        self::newStep($user, 'step2', 0);

        $this->newMessage($chat_id, self::getWord(2, $user->language_id, $user));

        return true;
    }




    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $post
     * @var integer $lang
     * @return boolean
     */
    public static function getWord($post, $lang, $user)
    {
        $text = Words::find()->where(['position' => $post, 'langs_id' => $lang])->one();
        return self::handleModel($text->value, $user);
    }


    /**
     * Обрабатывают строку по модели
     * @param string $text
     * @param \yii\base\Model|\yii\base\Model[] $model
     * @return string
     */
    public static function handleModel($text, $model)
    {
        $output = $text;

        $className = lcfirst(\yii\helpers\StringHelper::basename(get_class($model)));
        $tags = [];

        foreach ($model->attributes as $name => $value)
        {
            $tags['{$'."{$className}->{$name}".'}'] = $value;
        }

        $output = str_ireplace(array_keys($tags), array_values($tags), $output);

        return $output;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $chat_id
     * @var integer $message_id
     * @return boolean
     */
    public function setClear($chat_id, $message_id)
    {
        $this->getReq('editMessageReplyMarkup',[
            'chat_id'=> $chat_id,
            'message_id'=>$message_id,
            'text' => 'my text',
            'reply_markup' => null
        ]);
        return true;
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var integer $chat_id
     * @var string $text
     * @return boolean
     */
    public function getApply($chat_id, $text)
    {
        $user = UsersList::find()->where(['telegram_id' => $chat_id])->one();
        $this->getReq('sendMessage',[
            'chat_id'=> $chat_id,
            'text'=> $text,
            'reply_markup' => json_encode([
                'inline_keyboard' =>  [[['text' => self::getWord(33, $user->language_id, $user),'callback_data' => '+'],['text' => self::getWord(34, $user->language_id, $user),'callback_data' => '-']]],
            ])
        ]);
        return true;

    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var UsersList $user
     * @var string $text
     * @return boolean
     */
    public static function newStep($user, $text, $status)
    {
        $chat = new Chat([
            'chat_id' => $user->telegram_id,
            'user_id' => $user->id,
            'text' => $text,
            'date_time' => date('Y-d-m H:m:s'),
            'is_read' => $status,
        ]);

        $chat->save(false);


        $user->training_stage = $text;
        $user->save(false);
        return true;
    }


    public static function sendTelMessage($userId, $text)
    {
        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy_server = Settings::find()->where(['key' => 'proxy'])->one()->value;

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
            //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }



    public static function genCode()
    {

        $length = 5;
        $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

        do {
            $string ='';
            for( $i = 0; $i < $length; $i++) {
                $string .= $chars[rand(0,strlen($chars)-1)];
            }
        } while ( UsersList::find()->where(['partner_code' => $string])->one() != null );

        return $string;
    }



    public function newMessage($chat_id, $text)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $params = [
            'chat_id'=> $chat_id,
            'text'=> $text,
        ];
        $method = 'sendMessage';

        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy = Settings::find()->where(['key' => 'proxy'])->one()->value;

        $url =  "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if(count($params)){
            $url=$url.'?'.http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result, true);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function getReq($method,$params=[],$decoded=0)
    { //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.

        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy = Settings::find()->where(['key' => 'proxy'])->one()->value;

        $url =  "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if(count($params)){
            $url=$url.'?'.http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result, true);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function actionWebhook()
    {
        $token = Settings::find()->where(['key' => 'telegram_key'])->one()->value;//$token = '709328751:AAGWl7PwR5qg2uUlKmz7petTIfRtPBI9oBc';
        $proxy = Settings::find()->where(['key' => 'proxy'])->one()->value;

        $url = 'https://api.telegram.org/bot'.$token.'/setWebhook?url=https://assistent.teo-crm.ru/api/botinfo/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        echo $curl_scraped_page;
    }

    /**
     * @param $text
     * @return bool|string
     */
    public function actionInfoall()
    {
        $allpush = Alert::find()->where(['status' => false])->all();
        /** @var Alert $push */

        foreach ($allpush as $push) {
            $allResume = Resume::find()->where(['connect_telegram' => true,'questionary_id' => $push->questionary_id])->all();
            echo "____{$push->id}";
            $text = "
Новое предложение: {$push->name}

{$push->text}";

            /** @var Resume $resume */
            $i = 0;
            foreach ($allResume as $resume) {
                $i++;
                echo $this->sendadmin(['chat_id' => $resume->telegram_chat_id, 'parse_mode'=>'HTML', 'text' => $text]);
                echo "<br/> $resume->id";
            }
            $a = "По рассылке {$push->name} отправленно {$i} кол-во пользователям";
            $a .= $text;
            $this->sendadmin(['chat_id' => '247187885', 'parse_mode' => 'HTML', 'text' => $a]);
            $push->status = true;
            $push->save();
        }
    }


    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function actionSendRefer()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $headers = Yii::$app->response->headers;
        $headers->add('Access-Control-Allow-Origin', '*');

        $refer = new ReferalRedirects([
            'ip' => $_SERVER['REMOTE_ADDR'],
            'refer_company_id' => $_GET['ref'],
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
        ]);

        return ['result' => $refer->save()];
    }



}
