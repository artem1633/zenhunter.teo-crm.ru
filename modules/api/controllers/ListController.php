<?php

namespace app\modules\api\controllers;

use app\models\AccountingReport;
use app\models\AdditionalInformation;
use app\models\Channels;
use app\models\DailyReport;
use app\models\Reports;
use app\models\Settings;
use app\models\Tasks;
use app\models\Users;
use Codeception\PHPUnit\ResultPrinter\Report;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\Name;
use app\models\UserAgent;
use app\models\Proxy;
use yii\web\Response;
use app\models\Logs;
use app\models\Surname;
use app\models\AutoRegs;

/**
 * Default controller for the `api` module
 */
class ListController extends Controller
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['channels', 'reports','task', 'proxy', 'surnames', 'logs', 'auto-regs', 'akk-info'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [ ]);
    }

    public function actionChannels()
    {
//        $proxy = file_get_contents('https://dashboard.airsocks.in/partner?u=artem.kovalskiy.93@mail.ru&p=nOrlcE');
//        $proxy = json_decode($proxy);
//        foreach ($proxy->data as $item) {
//            $proxy = $item->hostIp.':'.$item->ports->http;
//            break;
//        }
        $setting = Settings::find()->where(['key' => 'using_proxy'])->one()->value;
        $chanelAll = Channels::find()->all();
        $proxy = json_decode(file_get_contents("https://proxy6.net/api/{$setting}/getproxy"),true);
        $list = $proxy['list'];
        foreach ($list as $item) {
            $proxy2[] = "{$item['ip']}:{$item['port']}";
        }
        $result['proxy'] = $proxy2;
        /** @var Channels $item */
        foreach ($chanelAll as $item) {
            $result['list'][] = [
                'channel_id' => $item->id,
                'project_id' => $item->project_id,
                'link' => $item->link,
            ];
        }

//        self::sendTelMessage($user_push, "Запрос к задаче {$task->name} - {$task->id}");
        return $result;
    }


    public function actionReports()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

//        $a = serialize($request);
//        self::sendTelMessage(['247187885','314811066'], "Новый запрос {$a}");
        $chan = Reports::find()->where(['link' => $request->post('link')])->one();
        if ($chan) {
            $setting = Settings::find()->where(['key' => 'time_interval'])->one()->value;
            $date = date('Y-m-d H:i', strtotime("-{$setting} days"));
            if ($chan->create_at > $date) {
                /** @var Reports $chan */
                $chan->type = $request->post('type');;
                $chan->link = $request->post('link');
                $chan->title = $request->post('title');
                $chan->date = $request->post('date');
                $chan->view_count = $request->post('view_count');
                $chan->reading_count = $request->post('reading_count');
                $chan->reading_prosent = $request->post('reading_prosent');
                $chan->average_time = $request->post('average_time');
                $chan->like_count = $request->post('like_count');
                $chan->comment_count = $request->post('comment_count');
                $chan->status = 1;
                $chan->project_id = $request->post('project_id');
                $chan->channel_id = $request->post('channel_id');
                $chan->update_at = date('Y-m-d H:i');
                $chan->save(false);
            }
        } else {
            /** @var Reports $chan */
            $chan = new Reports();
            $chan->type = $request->post('type');;
            $chan->link = $request->post('link');
            $chan->title = $request->post('title');
            $chan->date = $request->post('date');
            $chan->view_count = $request->post('view_count');
            $chan->reading_count = $request->post('reading_count');
            $chan->reading_prosent = $request->post('reading_prosent');
            $chan->average_time = $request->post('average_time');
            $chan->like_count = $request->post('like_count');
            $chan->comment_count = $request->post('comment_count');
            $chan->status = 1;
            $chan->project_id = $request->post('project_id');
            $chan->channel_id = $request->post('channel_id');
            $chan->create_at = date('Y-m-d H:i');
            $chan->save();
        }

        return ['message' => 'Успешно выполнено'];
    }

    public function actionAutoRegs()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $task = Tasks::findOne($request->post('task_id'));
        $company = Users::findOne($task->user_id);

        $reg = new AutoRegs();
        $reg->task_id = $request->post('task_id');
        $reg->login = $request->post('login');
        $reg->password = $request->post('password');
        $reg->link = $request->post('link');
        $reg->status = $request->post('status');
        $reg->gender = $request->post('gender');
        $reg->add_information = $request->post('add_information');
        $reg->proxy_type = (int) $request->post('proxy_type');
        $reg->comment = $request->post('сomment');
        $reg->register_date = date('Y-m-d H:i:s');
        $reg->user_id = $task->user_id;
        $reg->price = $request->post('price');

        $user_push =  explode(',', Settings::findByKey('user_push')->value);
        self::sendTelMessage($user_push, "Новый авторег {$reg->login}:{$reg->password} - {$reg->link}");

        if($reg->save(false)){
            $price = Settings::findByKey('cost_autoreg')->value;
            $pay = new AccountingReport([
                'date_report' => date('Y-m-d H:i:s'),
                'company_id' => $task->user_id,
                'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                'amount' => $price,
                'description' => "За регистрацию акк {$reg->login} себе стоимость {$reg->price}"
            ]);

            if (!$pay->save(false)){
                $a = serialize($pay->errors);
                self::sendTelMessage($user_push, "Ошибка {$a}");
                return $pay->errors;
            }

            $company->main_balance = $company->main_balance - $price;
            if (!$company->save(false)){
                $a = serialize($company->errors);
                self::sendTelMessage($user_push, "Ошибка {$a}");
                return $company->errors;
            }

            return ['message' => 'Успешно выполнено'];
        } else {
            $a = serialize($reg->errors);
            self::sendTelMessage($user_push, "Ошибка {$a}");
            return $reg->errors;
        }
    }


    public static function sendTelMessage($userId, $text)
    {
        $token = '876862739:AAGd7d_NtnTt8Eng5wGHbNe96jexdhigsXY';
        $proxy_server = '213.230.109.126:8080';

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
            //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }


    /**
     * @param array $data
     * @return mixed
     */
    public static function actionTest()
    {
        $data = [
            'type' => '1',
            'comment' => 'adfsf',
        ];
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://avtoreg.teo-crm.ru/api/list/logs');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }




    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    public function actionProxy()
    {
        $proxy = json_decode(file_get_contents('https://proxy6.net/api/0128ce7ba0-744d28a1d2-ee0618dbaf/getproxy'),true);
        $list = $proxy['list'];
        foreach ($list as $item) {
            var_dump($item['ip']);
            var_dump($item['port']);
        }
//        for($i = 0; $i < count($list); $i++) {
//            var_dump($list);
//        }
        return $list;
    }


}
